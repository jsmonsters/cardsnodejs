const express = require('express');
const path = require('path');
const app = express();
const port = 3001;

app.use(express.static(path.resolve(__dirname + "/static")))

//app.get('/', function (req, res) {
//    res.sendFile(path.resolve(__dirname, 'static', 'index.html'))
//})


app.get('/login', function (req, res) {
    res.sendFile(path.resolve(__dirname, 'static', 'login.html'))
})

app.get('/signup', function (req, res) {
    res.send()
})

app.listen(port, () => {
    console.log(`Example app listening at http://localhost:${port}`)
})